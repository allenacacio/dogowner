class UserNotifier < ActionMailer::Base
  default :from => 'admin@gmail.com'

  # send a signup email to the user, pass in the user object that   contains the user's email address
  def send_email(user,dog)
    puts "sending email from user...."
    @user = user
    @dog = dog
    mail( :to => @user.email,
          :subject => "Dog #{@dog.name} has been successfully registered." )
  end
end