class UsersController<ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]

  def index
    @page_header = "Reports (click on a row to show Dog details)"
    @users = User.all

  end
  def new
    @page_header = "Please Register"
    @page_button = "Register"
    @user = User.new
  end
  def create
    @user = User.new(user_params)
    if @user.save
      flash[:success] = "User has been successfully created."
        session[:user_id] = @user.id
        redirect_to "/"
    else
      flash[:error] = "Unable to register. Please try again."
      redirect_to :back
    end
  end
  def show
    @page_header = "User Profile"
  end
  def edit
    @page_header = "Manage Profile"
    @page_button = "Update"
  end
  def update
    if params[:user][:password].empty?
      user_update = @user.update_columns(user_params)
    else
      user_update = @user.update(user_params)
    end

    if user_update
      flash[:success] = "Profile has been successfully updated."
    else
      flash[:error] = "Unable to register. Please try again."
    end
    redirect_to @user
  end
  def destroy
    @user.destroy
      session[:user_id] = @user.id
      redirect_to "/"
  end

  private

  def set_user
    @user = User.find(params[:id])
  end

  def user_params
    u = User.find_by_username(params[:user][:username])
    role_id = 2
    role_id = params[:user][:role_id] if current_user && current_user.role_id == 1
    params_require = params.require(:user).permit(:firstname,:lastname,:address,:email,:username)
    additional_params = {:role_id=>role_id}
    additional_params[:password] = params[:user][:password] unless params[:user][:password].empty?
    result = params_require.merge(additional_params) do |key, oldval, newval|
        oldval ||= {}
        oldval.merge newval
    end
    result.permit!
  end
end