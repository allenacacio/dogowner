class DogsController<ApplicationController
  before_action :set_dog, only: [:show, :edit, :update, :destroy, :email]
  before_action :set_dog_registration

  def index
    @page_header = "Dogs List"
    if current_user.role_id == 1
      @dogs = Dog.all
    else
      @dogs = current_user.dogs()
    end

  end
  def new
    @dog = Dog.new
    @page_header = "Add New Dog"
    @page_button = "Add Dog"
  end
  def email
    @user = current_user
  end
  def create
    @dog = Dog.new(dog_params)
    if @dog.save
      flash[:success] = "Dog has been successfully created."
      UserNotifier.send_email(current_user,@dog).deliver if $send_notification
      redirect_to @dog
    else
      flash[:error] = "Unable to add dog. Please try again."
      redirect_to :back
    end
  end
  def show
    @page_header = "Dog Details"
    @user = current_user
  end
  def edit
    @page_header = "Edit Dog"
    @page_button = "Update"
  end
  def update
    if @dog.update(dog_params)
      flash[:success] = "Dog Profile has been successfully updated."
      UserNotifier.send_email(current_user,@dog).deliver if $send_notification
    else
      flash[:error] = "Unable to update dog profile. Please try again."
    end
    redirect_to @dog
  end
  def destroy
    @dog.destroy
    redirect_to dogs_path
  end


  private

  def set_dog
    @dog = Dog.find(params[:id])
  end

  def dog_params
    reg_map = RegistrationMapping.find_by_months(params[:dog][:registration_months])
    user_id = current_user.id
    dog = Dog.find_by_name(params[:dog][:name])
    #user_id = dog.user_id unless dog.nil?
    $send_notification = false
    unless params[:dog][:registration_months].nil?
      reg_cost = reg_map.cost
      is_email_sent = true
      $send_notification = true
    end
    params.require(:dog).permit(:name,:breed,:birthdate,:is_registered,:registration_months).merge(registration_cost: reg_cost, is_email_sent: is_email_sent, user_id: user_id)
  end
  def set_dog_registration
    @reg_map = RegistrationMapping.all
  end
end