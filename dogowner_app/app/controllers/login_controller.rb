class LoginController<ApplicationController
  def index
    session[:user_id] = nil
    @page_header = "Please Login"
    flash = nil

  end
  def create
    @user = User.authenticate(params[:username],params[:password])
    if @user
      puts "user is authenticated..."
      session[:user_id] = @user.id
      redirect_to dogs_path
    else
      puts "user is NOT authenticated..."
      flash[:error] = "User NOT Authenticated. Please try again."
      redirect_to "/"
    end
  end
  def destroy
    session[:user_id] = nil
    redirect_to "/"
  end
end