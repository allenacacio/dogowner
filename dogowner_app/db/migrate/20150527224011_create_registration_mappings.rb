class CreateRegistrationMappings < ActiveRecord::Migration
  def change
    create_table :registration_mappings do |t|
      t.integer :months
      t.decimal :cost

      t.timestamps null: false
    end
  end
end
