class AddEmailSentToDogs < ActiveRecord::Migration
  def change
    add_column :dogs, :is_email_sent, :boolean
  end
end
