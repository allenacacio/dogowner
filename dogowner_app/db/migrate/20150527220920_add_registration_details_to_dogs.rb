class AddRegistrationDetailsToDogs < ActiveRecord::Migration
  def change
    add_column :dogs, :is_registered, :boolean
    add_column :dogs, :registration_months, :integer
    add_column :dogs, :registration_cost, :decimal
  end
end
