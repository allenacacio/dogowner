# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150528104457) do

  create_table "dogs", force: :cascade do |t|
    t.string   "name"
    t.string   "breed"
    t.date     "birthdate"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.boolean  "is_registered"
    t.integer  "registration_months"
    t.decimal  "registration_cost"
    t.boolean  "is_email_sent"
    t.integer  "user_id"
  end

  create_table "registration_mappings", force: :cascade do |t|
    t.integer  "months"
    t.decimal  "cost"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "roles", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "firstname"
    t.string   "lastname"
    t.string   "address"
    t.string   "email"
    t.string   "password_hash"
    t.string   "password_salt"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.string   "username"
    t.integer  "role_id"
  end

end
