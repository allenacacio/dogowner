# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
Role.create ([{ name: 'Admin'},{name:'User'}])
user = User.new(:firstname=>'admin',:lastname=>'admin',:address=>'admin',:email=>'admin@admin.com',:username=>'admin',:password=>'123456',:password_confirmation=>'123456',:role_id=>1)
user.save
RegistrationMapping.create ([{ months: 3, cost: 25},{ months: 6, cost: 65}, { months: 12, cost: 85}])
